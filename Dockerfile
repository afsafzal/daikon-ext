#
# * installs Daikon 5.5.0, along with custom architectural invariant templates
# * instruments the C++ source code for the TurtleBot (TODO: add DynComp)
#
FROM christimperley/turtlebot:source
MAINTAINER Chris Timperley "christimperley@googlemail.com"

# Install prerequisites
RUN apt-get update && \
    apt-get install -y  rsync \
                        unzip \
                        texlive \
                        texinfo \
                        netpbm \
                        automake \
                        binutils-dev &&\
    apt-get clean && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# Install vanilla Daikon 5.5.0 to /daikon/daikon-5.5.0
ENV DAIKONDIR /daikon/daikon-5.5.0
RUN mkdir /daikon && cd /daikon &&\
    wget -q https://github.com/codespecs/daikon/archive/v5.5.0.tar.gz &&\
    tar -xf v5.5.0.tar.gz &&\
    rm -f v5.5.0.tar.gz

# Modify Daikon source to include new template
RUN rm "${DAIKONDIR}/java/daikon/Daikon.java"
ADD daikon_templates/ArchitectureInvariant.java \
    "${DAIKONDIR}/java/daikon/inv/unary/string"
ADD docker/Daikon.java "${DAIKONDIR}/java/daikon/Daikon.java"

# Install Java 7 JDK
RUN apt-get update --fix-missing && \
    apt-get install -y openjdk-7-jdk &&\
    apt-get clean && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
ENV JAVA_HOME /usr/lib/jvm/java-7-openjdk-amd64

# Rebuild from source
RUN make -C "$DAIKONDIR" rebuild-everything

# Update environmental variables
ENV DAIKONBIN "${DAIKONDIR}/bin"
ENV DAIKONSCRIPTS "${DAIKONDIR}/scripts"
ENV PLUMEBIN "${DAIKONDIR}/plume-lib/bin"
ENV PATH "${DAIKONBIN}:${DAIKONSCRIPTS}:${PLUMEBIN}:${PATH}"
ENV CLASSPATH "${DAIKONDIR}/daikon.jar:${CLASSPATH}"

# Instrument the source code for the TurtleBot (-gdwarf-2 -O0)
RUN echo 'set(CMAKE_CXX_FLAGS "-gdwarf-2 -O0 \${CMAKE_CXX_FLAGS}")' >> ${catkin_ws}/src/CMakeLists.txt
RUN bash -c "source /opt/ros/${ROS_DISTRO}/setup.bash && catkin_make"

# Install rosbagtotrace
ADD bagfiles /bagfiles
ADD rosbagtotrace /tmp/rosbagtotrace
RUN cd /tmp/rosbagtotrace && \
    python2.7 setup.py install && \
    cd / && \
    rm -rf /tmp/*
