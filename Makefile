all: build

build:
	docker build -t christimperley/brass:invariants .

push:
	docker push christimperley/brass:invariants

.PHONY: build push
