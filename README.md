# Daikon extension for ROS systems

This project tries to extend [Daikon](https://plse.cs.washington.edu/daikon/download/doc/daikon.html) to infer 
invariants on ROS systems based on the messages passed among nodes 

### TODO

* Need to account for debugging/recording nodes and topics

### Requirements

* ROS
You need to have ROS up and running in your system. Find the instruction of how to install ROS on your system [here](http://wiki.ros.org/indigo/Installation)
* Daikon
You need to install Daikon by source to be able to modify and extend it later. [Instruction](https://plse.cs.washington.edu/daikon/download/doc/daikon.html#Unix_002fLinux_002fMacOSX-installation)

### Modifications

Need to describe what the changes to ROS are, why you made them (e.g. to log services)
and what their limitations are.

### Set-up

* Follow the instructions at daikon_templates/README.txt to extend Daikon
* Change directory to: */opt/ros/***version of ROS installed***/lib/python2.7/dist-packages/rospy/impl/*
* Open the file *tcpros_service.py* with any text editor (You may need to open it using sudo)
* Look for line 681 and find:
```
#!python

def __init__(self, name, service_class, handler,
                 buff_size=DEFAULT_BUFF_SIZE, error_handler=None):
```
 (Line number may vary by ROS version). Change the name parameter to Name. It should be looking like this:


```
#!python

def __init__(self, Name, service_class, handler,
                 buff_size=DEFAULT_BUFF_SIZE, error_handler=None):
```


* Right below the parameters add the following lines of code:
    
```
#!python
name = Name
status =  name.split('/')[-1] 
if status != '~set_logger_level':
    if status != '~get_loggers':
        if name.split('___')[0] != 'OPERATOR':
            name = name + "_prime"
        else:
            name = name.split('___')[1]
        
```

* Save and exit 
* Then we need to modify the c++ equivalent. Change directory to: */opt/ros/***version of ROS installed***/include/ros/*
* Open the file *node_handle.h* with any text editor (You may need to open it using sudo)
* Look from line 877 to  1236 and modify each advertiseService:

```
#!c++

template<class T, class MReq, class MRes>
  ServiceServer advertiseService(const std::string& service, bool(T::*srv_func)(MReq &, MRes &), T *obj)
```
* Change the service parameter to Service. It should be looking like this: 
```
#!c++

template<class T, class MReq, class MRes>
  ServiceServer advertiseService(const std::string& Service, bool(T::*srv_func)(MReq &, MRes &), T *obj)
```
* Now for each advertiseService you must add the following lines of code under its parameters:

```
#!c++

std::string service = Service;
    std::string prime = "_prime";
    service = service + prime;
```
* At the end it should look like this:

```
#!c++

template<class T, class MReq, class MRes>
  ServiceServer advertiseService(const std::string& Service, bool(T::*srv_func)(MReq &, MRes &), T *obj)
  {

    std::string service = Service;
    std::string prime = "_prime";
    service = service + prime;

    AdvertiseServiceOptions ops;
    ops.template init<MReq, MRes>(service, boost::bind(srv_func, obj, _1, _2));
    return advertiseService(ops);
  }

```

* Save and exit after you have done the same with each advertiseService


* Change your directory to catkin_ws and run catkin_make. After that run "source devel/setup.sh". This step allows ROS core to find the recording nodes.
* Run "roslaunch recorder record.launch". The resultant .bag file is going to be stored in ~/.ros as Record.bag. You may want to change the destination by modifying daikon-ext/catkin_ws/src/recorder/launch/record.launch. 

```
#!xml
 <node pkg="rosbag" type="record" name="rosbag_record"
       args="-a -O Record.bag"/>


```
prior to Record.bag add the destination. Example: ~/daikon-ext/Record.bag

* You can skip the previous step and use the sample bag file in bagfiles directory.

### Running ###

* run "python2.7 trace_translation <bagfile>"
* "gzip output.dtrace"
* "java daikon.Daikon output.dtrace.gzip"
