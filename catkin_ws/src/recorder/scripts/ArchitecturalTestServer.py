#!/usr/bin/env python

import rospy 
import os
import sys
import rosservice 

def handle_deAttacher(req):
	print "In. Information recieved: %s "%(req.request)
	print "Allowing: %s" + %str(rospy.get_topic_class(req.request))
	return DeAttacher("Out")

def deAttach():
	rospy.init_node('deAttacher_server')
	s = rospy.service('deAttacher', DeAttacher, handle_deAttacher)
	print "Ready."
	rospy.spin()

if __name__== '__main__':
	deAttach()
