#!/usr/bin/env python

import rospy
import rostopic
# from recorder.msg import Subscribers
from std_msgs.msg import String


# def talker():
#     pub = rospy.Publisher('chatter', String, queue_size=10)
#     rospy.init_node('talker', anonymous=False)
#     rate = rospy.Rate(10) # 10hz
#     while not rospy.is_shutdown():
#         hello_str = "hello world %s" % rospy.get_time()
#         rospy.loginfo(hello_str)
#         pub.publish(hello_str)
#         rate.sleep()


def arch_subscriber():
    pub = rospy.Publisher('rec/arch_sub', String, queue_size=10)
    rospy.init_node('arch_sub', anonymous=False)
    rate = rospy.Rate(10)
    last_publish = {}
    while not rospy.is_shutdown():
        topics = rospy.get_published_topics()
        new_publish = {}
        for topic, typ in topics:
            info = rostopic.get_info_text(topic)
            subscribers = parse_info(info)
            new_publish[topic] = set(subscribers)
        if last_publish != new_publish:
            p = {}
            for topic in new_publish.keys():
                p[topic] = list(new_publish[topic])
            last_publish = new_publish
            pub.publish(str(p))
        rate.sleep()


def parse_info(info):
    reached_sub = False
    subscribers = []
    for l in info.splitlines():
        if reached_sub and not l:
            break
        elif reached_sub:
            parts = l.split(' ')
            if len(parts) < 3:
                rospy.loginfo("Something is wrong here!")
                continue
            subscribers.append(parts[2])
        elif l.startswith('Subscribers:'):
            reached_sub = True
    return subscribers

if __name__ == '__main__':
    try:
        arch_subscriber()
    except rospy.ROSInterruptException:
        pass

