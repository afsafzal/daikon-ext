package daikon.inv.unary.string;

import daikon.*;
import daikon.inv.*;

import plume.*;

import java.util.*;

/*>>>
import org.checkerframework.checker.interning.qual.*;
import org.checkerframework.checker.nullness.qual.*;
import org.checkerframework.dataflow.qual.*;
import typequals.*;
*/

/**
 * Represents a string that contains only printable ascii characters (values 32
 * through 126 plus 9 (tab)
 */
public final class ArchitectureInvariant extends SingleString {
	// We are Serializable, so we specify a version to allow changes to
	// method signatures without breaking serialization. If you add or
	// remove fields, you should change this number to the current date.
	static final long serialVersionUID = 20160425L;

	/**
	 * Boolean. True iff PrintableString invariants should be considered.
	 **/
	public static boolean dkconfig_enabled = true;

	private static ArchDataBase max_db = new ArchDataBase();
	private static ArchDataBase min_db = new ArchDataBase();

	public ArchitectureInvariant(PptSlice slice) {
		super(slice);
	}

	public /* @Prototype */ ArchitectureInvariant() {
		super();
	}

	private static /* @Prototype */ ArchitectureInvariant proto = new /* @Prototype */ ArchitectureInvariant();

	/** Returns the prototype invariant for PrintableString **/
	public static /* @Prototype */ ArchitectureInvariant get_proto() {
		return (proto);
	}

	/** returns whether or not this invariant is enabled **/
	public boolean enabled() {
		return dkconfig_enabled;
	}

	/** instantiate an invariant on the specified slice **/
	public ArchitectureInvariant instantiate_dyn(
			/* >>> @Prototype PrintableString this, */ PptSlice slice) {
		return new ArchitectureInvariant(slice);
	}

	/** return description of invariant. Only Daikon format is implemented **/
	/* @SideEffectFree */ public String format_using(OutputFormat format) {
		String s = "";
		s += "max: " + max_db.toString() + "\n";
		s += "min: " + min_db.toString() + "\n";
		return s;
	}

	/** Check to see if a only contains printable ascii characters **/
	public InvariantStatus add_modified(/* @Interned */ String a, int count) {
		return check_modified(a, count);
	}

	/** Check to see if a only contains printable ascii characters **/
	public InvariantStatus check_modified(/* @Interned */ String a, int count) {
		HashMap<String, HashSet<String>> list = ArchDataBase.parseString(a);
		if (list == null)
			return InvariantStatus.FALSIFIED;
		max_db.union(list);
		min_db.intersect(list);
		return InvariantStatus.NO_CHANGE;
	}

	protected double computeConfidence() {
		return Invariant.CONFIDENCE_JUSTIFIED;
	}

	/**
	 * Returns whether or not this is obvious statically. The only check is for
	 * static constants which are obviously printable (or not) from their values
	 */
	/* @Pure */
	public /* @Nullable */ DiscardInfo isObviousStatically(VarInfo[] vis) {
		if (vis[0].isStaticConstant()) {
			return new DiscardInfo(this, DiscardCode.obvious, vis[0].name() + " is a static constant.");
		}
		return super.isObviousStatically(vis);
	}

	/* @Pure */ public boolean isSameFormula(Invariant o) {
		assert o instanceof ArchitectureInvariant;
		return true;
	}

}

class ArchDataBase {
	public HashMap<String, HashSet<String>> list = new HashMap<>();

	public static HashMap<String, HashSet<String>> parseString(String s) {
		try {
			s = s.substring(1, s.length()-1);
			HashMap<String, HashSet<String>> list = new HashMap<>();
			int index = s.indexOf(":");
			while(index != -1){
				String name = s.substring(0, index);
				int f = name.indexOf("'");
				int l = name.lastIndexOf("'");
				name = name.substring(f+1, l);
				String elems = s.substring(s.indexOf('[')+1, s.indexOf(']'));
				list.put(name, new HashSet<String>());
				String[] elements = elems.split(",");
				for( String str : elements){
					f = str.indexOf("'");
					l = str.lastIndexOf("'");
					list.get(name).add(str.substring(f+1, l));
				}
				s = s.substring(s.indexOf(']')+1);
				index = s.indexOf(':');
			}
			if(list.isEmpty())
				return null;
			return list;
		}
		catch (Exception e) {
			return null;
		}
	}

  @SuppressWarnings("unchecked")
	public void union(HashMap<String, HashSet<String>> newState) {
		HashMap<String, HashSet<String>> newList = new HashMap<String, HashSet<String>>();
		for (String s : newState.keySet()) {
			if (list.containsKey(s)) {
				HashSet<String> set = (HashSet<String>) list.get(s).clone();
				set.addAll(newState.get(s));
				newList.put(s, set);
			} else
				newList.put(s, newState.get(s));
		}
		list = newList;
	}

  @SuppressWarnings("unchecked")
	public void intersect(HashMap<String, HashSet<String>> newState) {
		HashMap<String, HashSet<String>> newList = new HashMap<String, HashSet<String>>();
		for (String s : newState.keySet()) {
			if (list.containsKey(s)) {
				HashSet<String> set = (HashSet<String>) list.get(s).clone();
				set.retainAll(newState.get(s));
				newList.put(s, set);
			} else
				newList.put(s, newState.get(s));
		}
		list = newList;
	}
	
	@Override
	public String toString() {
		String s = "{";
		for (String str : list.keySet()){
			s += str + ": [";
			for (String v :list.get(str))
				s += v + ", ";
			if(!list.get(str).isEmpty())
				s = s.substring(0, s.length()-2);
			s += "], ";
		}
		if(!list.isEmpty())
			s = s.substring(0, s.length()-2);
		s += "}";
		return s;
	}
}
