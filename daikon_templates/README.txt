You should put these templates in your Daikon folder:

ArchitectureInvariant: java/daikon/inv/unary/string/ArchitectureInvariant.java

You also need to add a new line to java/daikon/Daikon.java in setup_proto_invs function:

proto_invs.add (ArchitectureInvariant.get_proto());

Then recompile