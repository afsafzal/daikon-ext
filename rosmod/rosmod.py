#!/usr/bin/env python
import os
import sys


# Prints out a given message as an error and exits
def error(msg, code=1):
    print("ERROR: {}".format(msg))
    exit(code)


# Writes a file with the given data
def write_file(file, data):
    for line in data:
        file.write(line)
    file.close()


# Recieves a block description in the following format ::: mod # 680-690 @
# non-mod # 680-681 ::: returns the begining and ending lines of a block of
# code from the mod version of a file.
def get_mod_values(code_block_description):
    values = [0, 0]
    values[0] = int(
        code_block_description.split('@')[0].split('#')[-1].split('-')[0])
    values[1] = int(
        code_block_description.split('@')[0].split('#')[-1].split('-')[1])
    return values


# Recieves a block description in the following format ::: mod # 680-690 @
# non-mod # 680-681 ::: returns the begining and ending lines of a block of
# code from the non-mod version of a file.
def get_non_mod_values(code_block_description):
    values = [0, 0]
    values[0] = int(
        code_block_description.split('@')[-1].split('#')[-1].split('-')[
            -1].split(' ')[0])
    values[1] = int(
        code_block_description.split('@')[-1].split('#')[-1].split(' ')[
            1].split('-')[0])
    return values


# Checks if the current version of node_handle.h is modified. 
# TODO: to simple?
def check_node_handle_mod_ros(node_handle_file_data):
    if 'Service' in node_handle_file_data[879]:
        return True
    else:
        return False


# Checks if the current version of tcpros_service.py is modified.
def check_tcpros_service_mod_ros(tcpros_service_file):
    if 'Name' in tcpros_service_file[679]:
        return True
    else:
        return False


# Receives a modified version of tcpros_service code and returns a an
# unmodified version. NOTE: handled with list
def get_non_mod_tcpros_service(tcpros_service_file_data):
    lines_to_write = []
    for x in range(0, 679):
        lines_to_write.append(tcpros_service_file_data[x])
    full_path = os.path.realpath(__file__)
    with open(os.path.dirname(full_path) + '/rosmodfiles/tcpros_service.nonmod',
              'r') as tcpros_service_non_mod:
        tcpros_service_non_mod_data = tcpros_service_non_mod.readlines()
        for x in range(1, len(tcpros_service_non_mod_data)):
            lines_to_write.append(tcpros_service_non_mod_data[x])
        for x in range(get_mod_values(tcpros_service_non_mod_data[0])[1],
                       len(tcpros_service_file_data)):
            lines_to_write.append(str(tcpros_service_file_data[x]))
        tcpros_service_non_mod.close()
        return lines_to_write


# Receives the original version of tcpros_service code and returns a modified
#  version. NOTE: handled with list
def get_mod_tcpros_service(tcpros_service_file_data):
    lines_to_write = []
    for x in range(0, 679):
        lines_to_write.append(tcpros_service_file_data[x])
    full_path = os.path.realpath(__file__)
    with open(os.path.dirname(full_path) + '/rosmodfiles/tcpros_service.mod',
              'r') as tcpros_service_mod:
        tcpros_service_mod_data = tcpros_service_mod.readlines()
        for x in range(1, len(tcpros_service_mod_data)):
            lines_to_write.append(tcpros_service_mod_data[x])
        for x in range(get_non_mod_values(tcpros_service_mod_data[0])[1] + 1,
                       len(tcpros_service_file_data)):
            lines_to_write.append(str(tcpros_service_file_data[x]))
        tcpros_service_mod.close()
        return lines_to_write


# Receives the original version of node_handle code and returns a modified
# version. NOTE: handled with list
def get_mod_node_handle(node_handle_file_data):
    lines_to_write = []
    for x in range(0, 876):
        lines_to_write.append(node_handle_file_data[x])
    full_path = os.path.realpath(__file__)
    with open(os.path.dirname(full_path) + '/rosmodfiles/node_handle.mod',
              'r') as node_handle_mod:
        node_handle_mod_data = node_handle_mod.readlines()
        code_blocks, code_block_description = extract_code_blocks(
            node_handle_mod_data)
        for x in range(0, 8):
            for mod_line in code_blocks[x]:
                lines_to_write.append(mod_line)
            for y in range(get_non_mod_values(code_block_description[x])[0],
                           get_non_mod_values(code_block_description[x + 1])[
                               1] - 1):
                lines_to_write.append(node_handle_file_data[y])
    return lines_to_write


# Receives the a modified version of node_handle code and returns a
# unmodified  version. NOTE: handled with list
def get_non_mod_node_handle(node_handle_file_data):
    lines_to_write = []
    for x in range(0, 876):
        lines_to_write.append(node_handle_file_data[x])
    full_path = os.path.realpath(__file__)
    with open(os.path.dirname(full_path) + '/rosmodfiles/node_handle.nonmod',
              'r') as node_handle_non_mod:
        node_handle_non_mod_data = node_handle_non_mod.readlines()
        code_blocks, code_block_description = extract_code_blocks(
            node_handle_non_mod_data)
        for x in range(0, 8):
            for line in code_blocks[x]:
                lines_to_write.append(line)
            for y in range(get_mod_values(code_block_description[x])[1],
                           get_mod_values(code_block_description[x + 1])[
                               0] - 1):
                lines_to_write.append(node_handle_file_data[y])
    return lines_to_write


# Looks into the provided files with code blocks and links each block with a
# number. returns the code blocks and a code block description to modify
# node_handle file. NOTE: We can definitely do better than this.
def extract_code_blocks(node_handle_mod_data):
    code_blocks = {0: [], 1: [], 2: [], 3: [], 4: [], 5: [], 6: [], 7: [],
                   8: []}
    code_block_description = {0: '', 1: '', 2: '', 3: '', 4: '', 5: '', 6: '',
                              7: '', \
                              8: '::: x # 2146-2146 @ x # 2130-2130 :::'}
    current_block = 0
    current_line = 0

    for line in node_handle_mod_data:
        if '@' in line:
            code_block_description[current_block] = line
            current_block += 1
    current_block = 0
    for x in range(current_line, len(node_handle_mod_data)):
        if '@' in node_handle_mod_data[x]:
            x += 1
            for y in range(x, len(node_handle_mod_data)):
                if '@' not in node_handle_mod_data[y]:
                    code_blocks[current_block].append(node_handle_mod_data[y])
                    current_line += 1
                else:
                    x += current_line
                    current_line = 0
                    current_block += 1
                    break
    return code_blocks, code_block_description


# Modifies node_handle.h and tcpros_service.py to register services with _prime
# flag.
def mod_ros(ros_version):
    with open(get_tcpros_service_location(ros_version),
              'r') as tcpros_service_file:
        tcpros_service_file_data = tcpros_service_file.readlines()
        if check_tcpros_service_mod_ros(tcpros_service_file_data):
            print('tcpros_service.py is already modified.')
        else:
            file_data = get_mod_tcpros_service(tcpros_service_file_data)
            tcpros_service_file.close()
            write_file(open(get_tcpros_service_location(ros_version), 'w'),
                       file_data)

    with open(get_node_handle_location(ros_version), 'r') as node_handle_file:
        node_handle_file_data = node_handle_file.readlines()
        if check_node_handle_mod_ros(node_handle_file_data):
            print('node_handle.h is already modified.')
        else:
            file_data = get_mod_node_handle(node_handle_file_data)
            node_handle_file.close()
            write_file(open(get_node_handle_location(ros_version), 'w'),
                       file_data)


# Returns node_handle.h and tcpros_service.py to its original state. 			
def undo_mod_ros(ros_version):
    with open(get_tcpros_service_location(ros_version),
              'r') as tcpros_service_file:
        tcpros_service_file_data = tcpros_service_file.readlines()
        if not check_tcpros_service_mod_ros(tcpros_service_file_data):
            print('tcpros_service.py is not modified.')
        else:
            file_data = get_non_mod_tcpros_service(tcpros_service_file_data)
            tcpros_service_file.close()
            write_file(open(get_tcpros_service_location(ros_version), 'w'),
                       file_data)

    with open(get_node_handle_location(ros_version), 'r') as node_handle_file:
        node_handle_file_data = node_handle_file.readlines()
        if not check_node_handle_mod_ros(node_handle_file_data):
            print('node_handle.h is not modified.')
        else:
            file_data = get_non_mod_node_handle(node_handle_file_data)
            node_handle_file.close()
            write_file(open(get_node_handle_location(ros_version), 'w'),
                       file_data)


# TODO: Will this be the location in all cases?
# Returns the location of tcpros_service.py
def get_tcpros_service_location(ros_version):
    return '/opt/ros/' + ros_version + '/lib/python2.7/dist-packages/rospy' \
        '/impl/tcpros_service.py' # TODO: what is this meant to be? Is this missing a concat?


# Returns the location of node_handle.py
def get_node_handle_location(ros_version):
    return '/opt/ros/' + ros_version + '/include/ros/node_handle.h'


def main():
    ros_versions = {'k': 'kinetic', 'i': 'indigo'}
    options = ['mod', 'non-mod']
    if len(sys.argv) == 1:
        error('Please indicate what verison of ROS you are running. \n'
              'i for indigo and k for kinetic. Also indicate a desired task. \n'
              'mod to instrument ROS or non-mod set back to original'
              'state. \n'
              'Example: rosmod i mod \n'
              '         rosmod i non-mod')
    if len(sys.argv) < 2:
        error('Please indicate what version of ROS you are running. \n'
              'i for indigo and k for kinetic.')
    if len(sys.argv) < 3:
        error(
            'Please indicate what task you want to be performed. \
            mod or non-mod')

    if sys.argv[1] not in ros_versions:
        error('Please indicate a correct ros version k for kinetic and '
              'i for indigo.')
    if sys.argv[2] not in options:
        error('Please indicate a correct task. mod or non-mod')

    if sys.argv[2] == 'mod':
        mod_ros(ros_versions[sys.argv[1]])
    elif sys.argv[2] == 'non-mod':
        undo_mod_ros(ros_versions[sys.argv[1]])


if __name__ == "__main__":
    main()
