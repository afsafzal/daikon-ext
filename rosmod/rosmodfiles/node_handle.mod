::: mod # 877-886 @ non-mod # 877-884 :::
  template<class T, class MReq, class MRes>
  ServiceServer advertiseService(const std::string& Service, bool(T::*srv_func)(MReq &, MRes &), T *obj)
  {
    std::string service = Service;
    std::string prime = "_prime";
    service = service + prime;
    AdvertiseServiceOptions ops;
    ops.template init<MReq, MRes>(service, boost::bind(srv_func, obj, _1, _2));
    return advertiseService(ops);
  }
::: mod # 925-934 @ non-mod # 923-930 :::
  template<class T, class MReq, class MRes>
  ServiceServer advertiseService(const std::string& Service, bool(T::*srv_func)(ServiceEvent<MReq, MRes>&), T *obj)
  {
    std::string service = Service;
    std::string prime = "_prime";
    service = service + prime;
    AdvertiseServiceOptions ops;
    ops.template initBySpecType<ServiceEvent<MReq, MRes> >(service, boost::bind(srv_func, obj, _1));
    return advertiseService(ops);
  }
::: mod # 974-984 @ non-mod # 970-978 :::
  template<class T, class MReq, class MRes>
  ServiceServer advertiseService(const std::string& Service, bool(T::*srv_func)(MReq &, MRes &), const boost::shared_ptr<T>& obj)
  {
    std::string service = Service;
    std::string prime = "_prime";
    service = service + prime;
    AdvertiseServiceOptions ops;
    ops.template init<MReq, MRes>(service, boost::bind(srv_func, obj.get(), _1, _2));
    ops.tracked_object = obj;
    return advertiseService(ops);
  }
::: mod # 1024-1034 @ non-mod # 1018-1026 :::
  template<class T, class MReq, class MRes>
  ServiceServer advertiseService(const std::string& Service, bool(T::*srv_func)(ServiceEvent<MReq, MRes>&), const boost::shared_ptr<T>& obj)
  {
    std::string service = Service;
    std::string prime = "_prime";
    service = service + prime;
    AdvertiseServiceOptions ops;
    ops.template initBySpecType<ServiceEvent<MReq, MRes> >(service, boost::bind(srv_func, obj.get(), _1));
    ops.tracked_object = obj;
    return advertiseService(ops);
  }
::: mod # 1071-1080 @ non-mod # 1063-1070 :::
  template<class MReq, class MRes>
  ServiceServer advertiseService(const std::string& Service, bool(*srv_func)(MReq&, MRes&))
  {
    std::string service = Service;
    std::string prime = "_prime";
    service = service + prime;
    AdvertiseServiceOptions ops;
    ops.template init<MReq, MRes>(service, srv_func);
    return advertiseService(ops);
  }
::: mod # 1117-1126 @ non-mod # 1107-1114 :::
  template<class MReq, class MRes>
  ServiceServer advertiseService(const std::string& Service, bool(*srv_func)(ServiceEvent<MReq, MRes>&))
  {
    std::string service = Service;
    std::string prime = "_prime";
    service = service + prime;
    AdvertiseServiceOptions ops;
    ops.template initBySpecType<ServiceEvent<MReq, MRes> >(service, srv_func);
    return advertiseService(ops);
  }
::: mod # 1161-1172 @ non-mod # 1149-1158 :::
  template<class MReq, class MRes>
  ServiceServer advertiseService(const std::string& Service, const boost::function<bool(MReq&, MRes&)>& callback, 
                                 const VoidConstPtr& tracked_object = VoidConstPtr())
  {
    std::string service = Service;
    std::string prime = "_prime";
    service = service + prime;
    AdvertiseServiceOptions ops;
    ops.template init<MReq, MRes>(service, callback);
    ops.tracked_object = tracked_object;
    return advertiseService(ops);
  }
::: mod # 1209-1220 @ non-mod # 1195-1204 :::
  template<class S>
  ServiceServer advertiseService(const std::string& Service, const boost::function<bool(S&)>& callback, 
                                 const VoidConstPtr& tracked_object = VoidConstPtr())
  {
    std::string service = Service;
    std::string prime = "_prime";
    service = service + prime;
    AdvertiseServiceOptions ops;
    ops.template initBySpecType<S>(service, callback);
    ops.tracked_object = tracked_object;
    return advertiseService(ops);
  }
