::: mod # 680-690 @ non-mod # 680-681 :::
    def __init__(self, Name, service_class, handler,
                 buff_size=DEFAULT_BUFF_SIZE, error_handler=None):

        name = Name 
        status = name.split('/')[-1]
        if status != '~set_logger_level':
            if status != '~get_loggers':
                if name.split('___')[0] != 'OPERATOR':
                    name = name + "_prime"
                else:
                    name = name.split('___')[1]
